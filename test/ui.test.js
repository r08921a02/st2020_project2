const puppeteer = require('puppeteer');
const url = 'http://localhost:3000'
const openBrowser = true; 


// 1 - example
test('[Content] Login page title should exist', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    let title = await page.$eval('body > div > form > div:nth-child(1) > h3', (content) => content.innerHTML);
    expect(title).toBe('Login');
    await browser.close();
})

// 2 
test('[Content] Login page join button should exist', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    let button = await page.$eval('body > div > form > div:nth-child(4) > button', (content) => content.innerHTML);
    expect(button).toBe('Join');
    await browser.close();
})

// 3 - example
test('[Screenshot] Login page', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.screenshot({path: 'test/screenshots/login.png'});
    await browser.close();
})

// 4
test('[Screenshot] Welcome message', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'zoe');
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'myroom');
    await page.keyboard.press('Enter', {delay: 100}); 
    await page.waitFor(1000);
    await page.screenshot({ path: 'test/screenshots/Welcome_message.png' });
    await browser.close();
})

// 5
test('[Screenshot] Error message when you login without user and room name', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);

    await page.keyboard.press('Enter', {delay: 100}); 
    await page.waitFor(1000);
    await page.screenshot({ path: 'test/screenshots/Error_message.png' });
    await browser.close();
})

// 6
test('[Content] Welcome message', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'zoe',100);
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'myroom',100);
    await page.keyboard.press('Enter', {delay: 100}); 
    await page.waitFor(2000);
    await page.waitForSelector('#messages > li > div:nth-child(2) > p');    
    let message = await page.evaluate(() => {
        return document.querySelector('#messages > li > div:nth-child(2) > p').innerHTML;
    });

    //let message = await page.$eval('body > div:nth-child(2) > ol > li > div:nth-child(2) > p', (content) => content.innerHTML);
    expect(message).toBe('Hi zoe, Welcome to the chat app');
    await page.waitFor(500);

    await browser.close();
})

// 7 - example
test('[Behavior] Type user name', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'Zoe', 100);
    let userName = await page.evaluate(() => {
        return document.querySelector('body > div > form > div:nth-child(2) > input[type=text]').value;
    });
    expect(userName).toBe('Zoe');
    await page.waitFor(500);

    await browser.close();
})

// 8
test('[Behavior] Type room name', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'Myroom', 100);
    let userName = await page.evaluate(() => {
        return document.querySelector('body > div > form > div:nth-child(3) > input[type=text]').value;
    });
    expect(userName).toBe('Myroom');
    await page.waitFor(500);

    await browser.close();
})

// 9 - example
test('[Behavior] Login', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'Zoe', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'Myroom', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 

    await page.waitFor(1000);
    await page.waitForSelector('#users > ol > li');    
    let member = await page.evaluate(() => {
        return document.querySelector('#users > ol > li').innerHTML;
    });

    expect(member).toBe('Zoe');
    await page.waitFor(500);
    
    await browser.close();
})

// 10
test('[Behavior] Login 2 users', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'Zoe', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'Myroom', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 
    await page.waitFor(600);

    const browser2 = await puppeteer.launch({ headless: !openBrowser });
    const page2 = await browser.newPage();
    
    await page2.goto(url);

    await page2.type('body > div > form > div:nth-child(2) > input[type=text]', 'Elisa', {delay: 100});
    await page2.type('body > div > form > div:nth-child(3) > input[type=text]', 'Myroom', {delay: 100});
    await page2.keyboard.press('Enter', {delay: 100}); 
    await page2.waitFor(600);

    await page.waitForSelector('#users > ol > li');    
    await page2.waitForSelector('#users > ol > li');    

    let member = await page.evaluate(() => {
        return document.querySelector('#users > ol > li:nth-child(1)').innerHTML;
    });
    expect(member).toBe('Zoe');

    let member2 = await page2.evaluate(() => {
        return document.querySelector('#users > ol > li:nth-child(1)').innerHTML;
    });
    expect(member2).toBe('Zoe');

    let member3 = await page.evaluate(() => {
        return document.querySelector('#users > ol > li:nth-child(2)').innerHTML;
    });
    expect(member3).toBe('Elisa');

    let member4 = await page2.evaluate(() => {
        return document.querySelector('#users > ol > li:nth-child(2)').innerHTML;
    });
    expect(member4).toBe('Elisa');

    await page.waitFor(500);
    await page2.waitFor(500);
    
    await browser.close();
    await browser2.close();
})

// 11
test('[Content] The "Send" button should exist', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'Zoe', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'Myroom', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 
    await page.waitFor(1000);

    let button = await page.evaluate(() => {
        return document.querySelector('#message-form > button').innerHTML;
    });

    expect(button).toBe('Send');
    await page.waitFor(500);

    await browser.close();
})

// 12 
test('[Behavior] Send a message', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'Zoe', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'Myroom', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 
    await page.waitFor(2000);
    await page.type('#message-form > input[type=text]', 'Hello', {delay:100});
    await page.keyboard.press('Enter', {delay: 100}); 
    await page.waitFor(500);
    let message = await page.evaluate(() => {
        return document.querySelector('#messages > li:nth-child(2) > div:nth-child(2) > p').innerHTML;
    });
    expect(message).toBe('Hello');

    await page.waitFor(500);
    await browser.close();
})

// 13
test('[Behavior] John says "Hi" and Mike says "Hello"', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'Myroom', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 
    await page.waitFor(2000);
    await page.type('#message-form > input[type=text]', 'Hi', {delay:100});
    await page.keyboard.press('Enter', {delay: 100}); 
    let message = await page.evaluate(() => {
        return document.querySelector('#messages > li:nth-child(2) > div:nth-child(2) > p').innerHTML;
    });
    expect(message).toBe('Hi');
    
    const browser2 = await puppeteer.launch({ headless: !openBrowser });
    const page2 = await browser.newPage();
    
    await page2.goto(url);

    await page2.type('body > div > form > div:nth-child(2) > input[type=text]', 'Mike', {delay: 100});
    await page2.type('body > div > form > div:nth-child(3) > input[type=text]', 'Myroom', {delay: 100});
    await page2.keyboard.press('Enter', {delay: 100}); 
    await page2.waitFor(2000)
    await page2.type('#message-form > input[type=text]', 'Hello', {delay:100});
    await page2.keyboard.press('Enter', {delay: 100}); 
    let message2 = await page2.evaluate(() => {
        return document.querySelector('#messages > li:nth-child(2) > div:nth-child(2) > p').innerHTML;
    });
    let message3 = await page.evaluate(() => {
        return document.querySelector('#messages > li:nth-child(4) > div:nth-child(2) > p').innerHTML;
    });
    expect(message2).toBe('Hello');
    expect(message3).toBe('Hello');

    await page.waitForSelector('#users > ol > li');    
    await page2.waitForSelector('#users > ol > li');    

    let member = await page.evaluate(() => {
        return document.querySelector('#users > ol > li:nth-child(1)').innerHTML;
    });
    expect(member).toBe('John');

    let member2 = await page2.evaluate(() => {
        return document.querySelector('#users > ol > li:nth-child(1)').innerHTML;
    });
    expect(member2).toBe('John');

    let member3 = await page.evaluate(() => {
        return document.querySelector('#users > ol > li:nth-child(2)').innerHTML;
    });
    expect(member3).toBe('Mike');

    let member4 = await page2.evaluate(() => {
        return document.querySelector('#users > ol > li:nth-child(2)').innerHTML;
    });
    expect(member4).toBe('Mike');

    await page.waitFor(500);
    await page2.waitFor(500);
    
    await browser.close();
    await browser2.close();
})

// 14
test('[Content] The "Send location" button should exist', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'Zoe', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'Myroom', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 
    await page.waitFor(600);
    
    let button = await page.$eval('#send-location', (content) => content.innerHTML);
    expect(button).toBe('Send location');
    await page.waitFor(500);

    await browser.close();
})

// 15
test('[Behavior] Send a location message', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser});
    const page = await browser.newPage();
 		//await page.on('dialog', async dialog => {
    //	console.log(dialog.message());
    //	await dialog.accept();
  	//});  
    await page.goto(url);
    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'Zoe', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'Myroom', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100});
    await page.waitFor(2000);

		const client = await page.target().createCDPSession();
    // emulate location
    await client.send('Emulation.setGeolocationOverride', {
        latitude: 25.014947,
        longitude: 121.535549,
        accuracy: 100,
    });

		const context = browser.defaultBrowserContext();
		await context.overridePermissions(url, ['geolocation']);
    await page.click('button[id="send-location"]');
    await page.waitFor(1000);
		await page.waitForSelector('#messages > li:nth-child(2) > div:nth-child(2)> div');

    await browser.close();
})

